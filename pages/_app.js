import "../node_modules/bulma/bulma.sass";
import "../styles/global.sass";

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
