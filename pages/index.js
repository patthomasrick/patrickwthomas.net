import Link from "next/link";
import Date from "../components/date";
import Layout from "../components/layout";
import { ProjectCard } from "../components/project-card";
import WhoAmI from "../docs/who-am-i.md";
import { getSortedPostsData } from "../lib/posts";
import { getSortedProjectsData } from "../lib/projects";

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  const allProjectsData = getSortedProjectsData();
  return {
    props: {
      allPostsData,
      allProjectsData,
    },
  };
}

export default function Home({ allPostsData, allProjectsData }) {
  return (
    <Layout home title="Hi, I'm Patrick." subtitle="Who am I?">
      <WhoAmI />

      <h1>Projects</h1>
      <div className="row columns is-multiline">
        {allProjectsData.map(({ id, title, subtitle, preview }) => (
          <div className="column is-4" key={id}>
            <Link href={`/projects/${id}`}>
              <a>
                <ProjectCard
                  title={title}
                  subtitle={subtitle}
                  thumbnail_src={preview}
                />
              </a>
            </Link>
          </div>
        ))}
      </div>

      <h1>Blog</h1>
      <ul>
        {allPostsData.map(({ id, date, title }) => (
          <li key={id}>
            <Link href={`/posts/${id}`}>
              <a>
                <Date dateString={date} /> - {title}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  );
}
