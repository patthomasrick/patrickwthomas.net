import Link from "next/link";
import Date from "../../components/date";
import Layout from "../../components/layout";
import { getSortedProjectsData } from "../../lib/projects";

export async function getStaticProps() {
  const allProjectsData = getSortedProjectsData();
  return {
    props: {
      allProjectsData,
    },
  };
}

export default function Projects({ allProjectsData }) {
  return (
    <Layout
      home
      title="Projects"
      subtitle="Here's what I've been working on"
      date=""
    >
      <ul>
        {allProjectsData.map(({ id, date, title }) => (
          <li key={id}>
            <Link href={`/projects/${id}`}>
              <a>
                <Date dateString={date} /> - {title}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  );
}
