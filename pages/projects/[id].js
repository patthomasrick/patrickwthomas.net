import Layout from "../../components/layout";
import { getAllProjectIds, getProjectData } from "../../lib/projects";
import pagesStyle from "../pages.module.sass";

export default function Project({ projectData }) {
  return (
    <Layout title={projectData.title} subtitle={projectData.subtitle}>
      <div
        dangerouslySetInnerHTML={{ __html: projectData.contentHtml }}
        className={pagesStyle.pageContent}
      />
    </Layout>
  );
}

export async function getStaticPaths() {
  const paths = getAllProjectIds();
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const projectData = await getProjectData(params.id);
  return {
    props: {
      projectData,
    },
  };
}
