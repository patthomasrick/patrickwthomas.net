import Layout from "../../components/layout";
import { getAllPostIds, getPostData } from "../../lib/posts";
import pagesStyle from "../pages.module.sass";

export default function Post({ postData }) {
  return (
    <Layout title={postData.title} date={postData.date}>
      <div
        dangerouslySetInnerHTML={{ __html: postData.contentHtml }}
        className={pagesStyle.pageContent}
      />
    </Layout>
  );
}

export async function getStaticPaths() {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const postData = await getPostData(params.id);
  return {
    props: {
      postData,
    },
  };
}
