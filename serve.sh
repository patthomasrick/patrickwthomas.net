#!/bin/bash

source /home/thomas/.bashrc
cd /home/thomas/web/patrickwthomas.net || exit 1
npx next build
npx next start -p 8000
