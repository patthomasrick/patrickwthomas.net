const maxSummaryWords = 40;

export function toSummary(text) {
  // Convert text to a truncated summary of up to X words.
  var s = new String(text);
  var pos = 0;
  var count = 0;
  while (true) {
    if (pos > s.length) {
      break;
    } else if (s[pos] === " ") {
      count++;
    }

    if (count >= maxSummaryWords) {
      break;
    }

    pos++;
  }
  return s.slice(0, pos) + "...";
}
