# patrickwthomas.net

This is the source code for my personal website, [patrickwthomas.net](https://patrickwthomas.net/). This project uses NextJS in order to display a simple blog.

This from a starter template for [Learn Next.js](https://nextjs.org/learn). I highly recommend the tutorial for all levels of experience in JavaScript.
