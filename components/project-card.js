import styles from "./project-card.module.sass";

export function ProjectCard({ title, subtitle, thumbnail_src }) {
  return (
    <div className="card has-text-left">
      <div className="card-image">
        <figure className="image is-4by3 mx-0">
          <img src={thumbnail_src} alt={title} className={styles.imgCover} />
        </figure>
      </div>
      <div className="card-content content">
        <h3 className="title is-5">{title}</h3>
        <h3 className="subtitle is-7">{subtitle}</h3>
      </div>
    </div>
  );
}
