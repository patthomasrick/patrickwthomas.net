import Head from "next/head";
import Date from "./date";
import Footer from "./footer";
import Header from "./header";
import layoutStyle from "./layout.module.sass";
import Nav from "./navbar";

export default function Layout({ children, title, subtitle, date, home }) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <script src="/scripts/nav.js"></script>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/favicon/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon/favicon-16x16.png"
        />
        <link rel="manifest" href="/favicon/site.webmanifest" />

        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-5N4084FFJL"
        ></script>
        <script src="/scripts/gtag.js"></script>

        <script
          async
          defer
          data-domain="patrickwthomas.net"
          src="https://analytics.patrickwthomas.net/js/plausible.js"
        ></script>
      </Head>

      <Nav />
      <Header home={home} />
      <section
        className={`${layoutStyle.footerFix} container content is-max-desktop has-text-justified px-3`}
      >
        {title && <h1 className="title is-2 has-text-left">{title}</h1>}
        {subtitle && (
          <h2 className="subtitle is-4 has-text-left">{subtitle}</h2>
        )}
        {date && (
          <h3 className="subtitle is-4">
            <Date dateString={date} />
          </h3>
        )}
        {children}
      </section>

      <Footer />
    </>
  );
}
