import { faBox, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import utilStyle from "../styles/utils.module.sass";
import Socials from "./socials";

const navItems = [
  {
    dest: "/",
    label: "Home",
    icon: faHome,
  },
  {
    dest: "/projects",
    label: "Projects",
    icon: faBox,
  },
];

export default function Nav({}) {
  return (
    <nav
      className="navbar has-text-dark"
      role="navigation"
      aria-label="main navigation"
    >
      <a
        role="button"
        className="navbar-burger"
        data-target="navMenu"
        aria-label="menu"
        aria-expanded="false"
      >
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>

      <div className="navbar-menu" id="navMenu">
        <div className={`${utilStyle.fontSerif} navbar-start`}>
          {navItems.map(({ dest, label, icon }) => (
            <Link href={dest} key={dest}>
              <a className="navbar-item">
                <span className="icon">
                  <FontAwesomeIcon icon={icon} className="mr-2" />
                </span>
                {label}
              </a>
            </Link>
          ))}
        </div>

        <div className="navbar-end">
          <Socials />
        </div>
      </div>
    </nav>
  );
}
