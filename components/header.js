import layoutStyle from "./layout.module.sass";
import headerStyle from "./header.module.sass";

export default function Header({ home }) {
  return (
    <section className="hero">
      <div className="hero-body">
        <div className={`${home && headerStyle.fadeIn} container`}>
          <div className="columns is-vcentered is-centered">
            <div className="column-auto mr-3 has-text-centered-mobile">
              <div>
                <figure className="image is-128x128">
                  <img
                    src="/images/me.jpg"
                    alt="Me"
                    className={`${layoutStyle.headshot} is-rounded`}
                  />
                </figure>
              </div>
            </div>
            <div className="column-auto">
              <h1 className="title is-1 has-text-weight-light">
                Patrick Thomas
              </h1>
              <h2 className="subtitle is-3 has-text-weight-light">
                UVa BS CS, BS CpE 2021
              </h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
