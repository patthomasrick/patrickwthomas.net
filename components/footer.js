export default function Footer({}) {
  var date = new Date();
  return (
    <footer className="footer mt-5">
      <div className="content has-text-centered">
        <strong>Patrick Thomas</strong>, {date.getFullYear()}. Website made with{" "}
        <a href="https://nextjs.org/" target="_blank" rel="noopener noreferrer">
          Next.js
        </a>{" "}
        and{" "}
        <a href="https://bulma.io/" target="_blank" rel="noopener noreferrer">
          Bulma
        </a>
        . Check out the{" "}
        <a
          href="https://gitlab.com/patthomasrick/patrickwthomas.net"
          target="_blank"
          rel="noopener noreferrer"
        >
          source code on GitLab
        </a>
        .
      </div>
    </footer>
  );
}
