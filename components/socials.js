import {
  faGithub,
  faGitlab,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Social({ linkDest, icon }) {
  return (
    <a href={linkDest} target="_blank" rel="noopener noreferrer">
      <span className="icon is-medium has-text-dark mr-3">
        <FontAwesomeIcon icon={icon} className="fa-lg"></FontAwesomeIcon>
      </span>
    </a>
  );
}

export default function Socials({}) {
  return (
    <div className="navbar-item">
      <Social linkDest="https://github.com/patthomasrick" icon={faGithub} />
      <Social linkDest="https://gitlab.com/patthomasrick" icon={faGitlab} />
      <Social
        linkDest="https://www.linkedin.com/in/patrick-thomas-032b82196/"
        icon={faLinkedin}
      />
      <Social linkDest="mailto:patrick@patrickwthomas.net" icon={faEnvelope} />
    </div>
  );
}
