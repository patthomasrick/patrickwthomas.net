I’m Patrick, a fullstack software engineer at [Dr. Fit, Health and Wellness](https://drfit.io/). I graduated from the [University of Virginia’s School of Engineering and Applied Sciences](https://engineering.virginia.edu/) in May of 2021 with bachelor's degrees in Computer Science and Computer Engineering.

I have experience in the following languages and tools, listed in no particular order: Python, Java, C, C++, LaTeX, Javascript, HTML, PHP, JavaScript, Java, Scikit-Learn, Tensorflow, MySQL, Neo4j, Apache, Nginx, Linux, Docker, Vagrant, Homestead, VirtualBox, WSL, Bash/Shell scripting, Go, VHDL, MATLAB, LabView, x84-64 assembly, RISC-V assembly, AWS, GCP, cloud hosting, and more.

## Dr. Fit (2021 - Current)

In the summer of 2021 I was hired as a fullstack engineer for Dr. Fit. There, I develop their content management platform and develop service integrations.

## Research (2020 - 2021)

During undergrad I was on a research team under [Dr. Yuan Tian](https://engineering.virginia.edu/faculty/yuan-tian), where I helped build an automated analysis tool to recognize General Data Protection Regulations in WordPress plugin PHP code.

## University of Virginia (2017 - 2021)

I started my undergraduate degrees at UVa in 2017. The classes that I have taken have given me experience in a wide array of topics, ranging from HCI and UX design to compilers to hardware design in VHDL. I have a variety of interests, including web development, software testing, and low-level embedded programming, and more. At UVa, I was an active member of the [Cavalier Marching Band’s Drumline](https://marchingband.as.virginia.edu/) all throughout college and have also been a part of various music ensembles, such as the [UVa Wind Ensemble](https://music.virginia.edu/wind-ensemble).
